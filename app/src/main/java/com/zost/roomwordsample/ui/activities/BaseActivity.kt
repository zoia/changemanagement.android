package com.zost.roomwordsample.ui.activities

import android.os.AsyncTask
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import java.util.*
import javax.mail.Message
import javax.mail.PasswordAuthentication
import javax.mail.Session
import javax.mail.Transport
import javax.mail.internet.InternetAddress
import javax.mail.internet.MimeMessage

abstract class BaseActivity : AppCompatActivity() {

    class SendMailTask : AsyncTask<Message, String, String>() {
        override fun doInBackground(vararg messages: Message?): String {
            Transport.send(messages[0])
            return "Success"
        }
    }

    private val handler = Thread.UncaughtExceptionHandler { thread, ex ->

        val username = "zoeostapiuk@gmail.com"
        val password = "askaboutyachting30"

        val props = Properties()
        props["mail.smtp.auth"] = "true"
        props["mail.smtp.starttls.enable"] = "true"
        props["mail.smtp.host"] = "smtp.gmail.com"
        props["mail.smtp.port"] = "587"

        val session = Session.getInstance(props,
            object : javax.mail.Authenticator() {
                override fun getPasswordAuthentication(): PasswordAuthentication {
                    return PasswordAuthentication(username, password)
                }
            })

        try {

            val message = MimeMessage(session)
            message.setFrom(InternetAddress(username))
            message.setRecipients(
                Message.RecipientType.TO,
                InternetAddress.parse("ostapyukzoya@outlook.com")
            )
            message.subject = "Unhandled exception occured"
            val stacktrace = ex.stackTrace.joinToString(separator = "\n") { it.toString() }
            message.setText(ex.toString() + "\nStacktrace:\n" + stacktrace)

            SendMailTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, message).get()
        } catch (e: Exception) {
            throw RuntimeException(e)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //Thread.setDefaultUncaughtExceptionHandler(handler)
    }

}