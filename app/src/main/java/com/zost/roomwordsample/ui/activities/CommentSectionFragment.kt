package com.zost.roomwordsample.ui.activities

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.zost.roomwordsample.R
import com.zost.roomwordsample.databinding.CommentSectionFragmentBinding
import com.zost.roomwordsample.models.Comment
import com.zost.roomwordsample.models.User
import com.zost.roomwordsample.ui.adapters.CommentsAdapter
import com.zost.roomwordsample.viewmodels.CommentSectionViewModel
import kotlinx.android.synthetic.main.comment_section_fragment.view.*
import java.util.*

class CommentSectionFragment : Fragment() {

    companion object {
        fun newInstance(issueId: Int) =
            CommentSectionFragment().apply {
                arguments = Bundle().apply {
                    putInt(ARG_ISSUE_ID, issueId)
                }
            }

        fun getCommentUpdateIntent(
            commentId: Int,
            commentText: String,
            userFullName: String,
            userProfilePicture: String?
        ): Intent {
            return Intent(IssueCommentsActivity.COMMENT_UPDATE).apply {
                putExtra(COMMENT_TEXT, commentText)
                putExtra(COMMENT_ID, commentId)
                putExtra(COMMENT_CREATED_BY, userFullName)
                putExtra(PICTURE_URL, userProfilePicture)
            }
        }

        private const val ARG_ISSUE_ID = "IssueId"
        private const val COMMENT_TEXT = "comment_text"
        private const val COMMENT_ID = "comment_id"
        private const val COMMENT_CREATED_BY = "comment_created_by"
        private const val PICTURE_URL = "comment_created_by_picture_url"
    }

    lateinit var viewModel: CommentSectionViewModel

    private lateinit var broadcastManager: LocalBroadcastManager

    private val commentUpdateReceiver = object : BroadcastReceiver() {

        override fun onReceive(context: Context?, intent: Intent?) {
            if (intent == null) return

            if (intent.action == IssueCommentsActivity.COMMENT_UPDATE) {
                val commentText = intent.getStringExtra(COMMENT_TEXT)
                val commentId = intent.getIntExtra(COMMENT_ID, 0)
                val createdBy = intent.getStringExtra(COMMENT_CREATED_BY)
                val pictureUrl = intent.getStringExtra(PICTURE_URL)
                val commentEntity = Comment(
                    commentId,
                    commentText,
                    Date(),
                    User("", "", createdBy, "", pictureUrl)
                )

                val comments = viewModel.comments.value
                comments?.let {
                    viewModel.comments.value = listOf(commentEntity).plus(it)
                }
            }
        }

    }

    override fun onDestroy() {
        super.onDestroy()
        broadcastManager.unregisterReceiver(commentUpdateReceiver)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        broadcastManager = LocalBroadcastManager.getInstance(activity!!)
        val filter = IntentFilter().apply { addAction(IssueCommentsActivity.COMMENT_UPDATE) }
        broadcastManager.registerReceiver(commentUpdateReceiver, filter)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(CommentSectionViewModel::class.java)
        viewModel.loadComments(arguments!!.getInt(ARG_ISSUE_ID))
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding = DataBindingUtil.inflate<CommentSectionFragmentBinding>(
            inflater, R.layout.comment_section_fragment, container, false
        ).apply {
            setLifecycleOwner(this@CommentSectionFragment)
            viewModel = this@CommentSectionFragment.viewModel
        }

        binding.root.comments_list?.apply {
            layoutManager = LinearLayoutManager(activity)
            adapter = CommentsAdapter(emptyList())
        }

        return binding.root
    }

}
