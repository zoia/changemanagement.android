package com.zost.roomwordsample.ui.binding

import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import com.zost.roomwordsample.ui.adapters.BindableAdapter
import com.zost.roomwordsample.ui.adapters.ItemClickedListener

@BindingAdapter("app:data")
fun <T> setRecyclerViewProperties(recyclerView: RecyclerView, items: List<T>) {

    val adapter = recyclerView.adapter
    if (adapter is BindableAdapter<*>) {
        (adapter as BindableAdapter<T>).setData(items)
    }
}

@BindingAdapter("app:itemClickedListener")
fun <T> setItemClickedListener(recyclerView: RecyclerView, listener: ItemClickedListener<T>) {

    val adapter = recyclerView.adapter
    if (adapter is BindableAdapter<*>) {
        (adapter as BindableAdapter<T>).setItemClickedListener(listener)
    }
}