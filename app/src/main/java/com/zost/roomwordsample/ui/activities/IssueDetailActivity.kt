package com.zost.roomwordsample.ui.activities

import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import com.zost.roomwordsample.R
import com.zost.roomwordsample.http.Api
import kotlinx.android.synthetic.main.activity_issue_detail.*
import kotlinx.android.synthetic.main.app_toolbar.*

class IssueDetailActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_issue_detail)

        setSupportActionBar(app_toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = "Деталі посту"

        setShare()

        val issueId = intent.getIntExtra(IssueDetailFragment.ARG_ITEM_ID, 0)

        val fragment = IssueDetailFragment.newInstance(issueId, true)
        supportFragmentManager.beginTransaction()
            .replace(R.id.item_detail_container, fragment)
            .commit()
    }

    private fun setShare() {
        fab_share.setOnClickListener {
            val fragment = supportFragmentManager
                .findFragmentById(R.id.item_detail_container) as IssueDetailFragment

            val intent = Intent(android.content.Intent.ACTION_SEND)
            intent.type = "text/plain"
            intent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Деталі поста")

            val link = Api.ISSUE_DETAILS_URL + fragment.viewModel.issueId.value
            val message = "${fragment.viewModel.summary.value} (${fragment.viewModel.description.value}). $link"
            intent.putExtra(android.content.Intent.EXTRA_TEXT, message)
            startActivity(Intent.createChooser(intent, "Поділитись"))
        }
    }

    override fun onOptionsItemSelected(item: MenuItem) =
        when (item.itemId) {
            android.R.id.home -> {
                navigateUpTo(Intent(this, IssueListActivity::class.java))
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
}
