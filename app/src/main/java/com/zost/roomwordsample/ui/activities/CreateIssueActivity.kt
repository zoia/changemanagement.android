package com.zost.roomwordsample.ui.activities

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.databinding.DataBindingUtil
import androidx.databinding.Observable
import androidx.databinding.ObservableField
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.thejuki.kformmaster.helper.FormBuildHelper
import com.thejuki.kformmaster.helper.form
import com.thejuki.kformmaster.model.BaseFormElement
import com.thejuki.kformmaster.widget.FormElementMargins
import com.zost.roomwordsample.R
import com.zost.roomwordsample.databinding.ActivityCreateIssueBinding
import com.zost.roomwordsample.infrastructure.Event
import com.zost.roomwordsample.models.AttributeType
import com.zost.roomwordsample.models.IssueAttributeValue
import com.zost.roomwordsample.models.IssueTypeAttribute
import com.zost.roomwordsample.ui.custom.ValidatableAttribute
import com.zost.roomwordsample.ui.custom.datetime.FormMaterialDateTimeElement
import com.zost.roomwordsample.ui.custom.datetime.FormMaterialDateTimeViewBinder
import com.zost.roomwordsample.ui.custom.input.FormMaterialInputElement
import com.zost.roomwordsample.ui.custom.input.FormMaterialInputViewBinder
import com.zost.roomwordsample.ui.custom.materialDateTime
import com.zost.roomwordsample.ui.custom.materialInput
import com.zost.roomwordsample.ui.custom.spinner
import com.zost.roomwordsample.ui.custom.spinner.FormSpinnerElement
import com.zost.roomwordsample.ui.custom.spinner.FormSpinnerViewBinder
import com.zost.roomwordsample.viewmodels.CreateIssueViewModel
import kotlinx.android.synthetic.main.activity_create_issue.*
import kotlinx.android.synthetic.main.app_toolbar.*
import java.text.SimpleDateFormat
import java.util.*

class CreateIssueActivity : BaseActivity() {

    private lateinit var createIssueViewModel: CreateIssueViewModel

    private lateinit var builder: FormBuildHelper

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        createIssueViewModel = ViewModelProviders.of(this).get(CreateIssueViewModel::class.java)
        createIssueViewModel.loadIssueTypes()

        val binding: ActivityCreateIssueBinding =
            DataBindingUtil.setContentView(this, R.layout.activity_create_issue)
        binding.setLifecycleOwner(this)
        binding.viewModel = createIssueViewModel

        setSupportActionBar(app_toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)

        createIssueViewModel.validationRequested.observe(this,
            Observer<Event<Boolean>> {

                var allValid = true
                val validatedAttributes =
                    builder.elements.map {
                        if (it is ValidatableAttribute) {
                            if (!it.performValidation()) {
                                allValid = false
                            }

                            val result = IssueAttributeValue(it.attributeId)
                            when (it) {
                                is FormMaterialDateTimeElement -> result.dateValue = it.dateValue
                                is FormSpinnerElement -> result.attributeAllowedValueId = it.value?.id
                                is FormMaterialInputElement -> {
                                    if (it.numbersOnly) {
                                        result.numberValue = it.value?.toDoubleOrNull()
                                    } else {
                                        result.stringValue = it.value
                                    }
                                }
                            }

                            result
                        } else {
                            IssueAttributeValue(0)
                        }
                    }

                createIssueViewModel.validationPerformed.set(Event(if (allValid) validatedAttributes else null))
            })

        createIssueViewModel.issueTypes.observe(this,
            Observer {
                invalidateOptionsMenu()
            })

        createIssueViewModel.issueCreated.observe(this,
            Observer<Event<Int>> {
                onBackPressed()
            })

        createIssueViewModel.selectedIssueType.addOnPropertyChangedCallback(object :
            Observable.OnPropertyChangedCallback() {
            override fun onPropertyChanged(p0: Observable, p1: Int) {
                val field = p0 as ObservableField<DropDownItem>
                supportActionBar?.title = field.get()?.data
            }
        })

        createIssueViewModel.attributes.observe(
            this,
            Observer<List<IssueTypeAttribute>> { attributes ->

                builder = form(this, recyclerView) {
                    spinner(-1) {
                        values = createIssueViewModel.projects.value ?: emptyList()
                        required = true
                        clearable = true
                        margins = FormElementMargins(0, 0, 0, 8)
                        title = "Проект"
                    }
                    attributes.map {
                        when (it.attribute.attributeType) {

                            AttributeType.STRING -> {
                                materialInput(it.attributeId) {
                                    floatingLabel = it.attribute.label
                                    required = it.isRequired
                                    clearable = true
                                    margins = FormElementMargins(0, 0, 0, 8)
                                }
                            }
                            AttributeType.DATE -> {
                                materialDateTime(it.attributeId) {
                                    required = it.isRequired
                                    clearable = true
                                    dateValue = Date()
                                    dateFormat = SimpleDateFormat("MM/dd/yyyy hh:mm a", Locale.US)
                                    minimumDate = dateFormat.parse("01/01/2018 12:00 AM")
                                    maximumDate = dateFormat.parse("12/15/2025 12:00 PM")
                                    hint = it.attribute.label
                                    rightToLeft = false
                                    margins = FormElementMargins(0, 0, 0, 8)
                                }
                            }
                            AttributeType.NUMBER -> {
                                materialInput(it.attributeId) {
                                    floatingLabel = it.attribute.label
                                    required = it.isRequired
                                    numbersOnly = true
                                    clearable = true
                                    margins = FormElementMargins(0, 0, 0, 8)
                                }
                            }
                            AttributeType.SELECTABLE -> {
                                val options = it.attribute.attributeAllowedValues.map { attr ->
                                    DropDownItem(attr.id, attr.label)
                                }.toMutableList()
                                options.add(0, DropDownItem(null, "-"))

                                spinner(it.attributeId) {
                                    values = options
                                    required = it.isRequired
                                    clearable = true
                                    margins = FormElementMargins(0, 0, 0, 8)
                                    title = it.attribute.label
                                } as BaseFormElement<*>
                            }
                        }
                    }
                }

                builder.registerCustomViewBinder(FormMaterialInputViewBinder(this, builder).viewBinder)
                builder.registerCustomViewBinder(FormSpinnerViewBinder(this, builder).viewBinder)
                builder.registerCustomViewBinder(FormMaterialDateTimeViewBinder(this, builder).viewBinder)

            })
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.toolbar_menu, menu)
        return true
    }

    override fun onPrepareOptionsMenu(menu: Menu?): Boolean {
        menu?.clear()

        createIssueViewModel.issueTypes.value?.forEach {
            menu?.add(0, it.id!!, 0, it.data)
        }

        return super.onPrepareOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return when (item?.itemId) {
            android.R.id.home -> {
                onBackPressed()
                true
            }
            else -> {
                val vmItem = createIssueViewModel.issueTypes.value?.find { it.id == item?.itemId }
                return vmItem?.let {
                    createIssueViewModel.selectedIssueType.set(vmItem)
                    true
                } ?: super.onOptionsItemSelected(item)
            }
        }
    }


}
