package com.zost.roomwordsample.ui.custom.spinner

import android.widget.ArrayAdapter
import android.widget.Spinner
import com.github.vivchar.rendererrecyclerviewadapter.ViewHolder
import com.thejuki.kformmaster.R
import com.thejuki.kformmaster.state.BaseFormViewState
import com.zost.roomwordsample.ui.activities.DropDownItem

class FormSpinnerViewState(holder: ViewHolder) : BaseFormViewState(holder) {

    private var dropDownItem: DropDownItem? = null

    init {
        val spinner = holder.viewFinder.find(R.id.formElementValue) as Spinner
        dropDownItem = spinner.selectedItem as DropDownItem
    }

    override fun restore(holder: ViewHolder) {
        super.restore(holder)

        val spinner = holder.viewFinder.find(R.id.formElementValue) as Spinner
        val adapter = spinner.adapter as ArrayAdapter<DropDownItem>

        val position = adapter.getPosition(dropDownItem)
        if (position != -1) {
            spinner.setSelection(position)
        }
    }

}