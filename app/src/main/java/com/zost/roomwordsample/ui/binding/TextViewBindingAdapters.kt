package com.zost.roomwordsample.ui.binding

import android.widget.TextView
import androidx.databinding.BindingAdapter
import java.text.SimpleDateFormat
import java.util.*

@BindingAdapter("app:date")
fun setDate(input: TextView, date: Date) {

    input.text = SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(date)
}