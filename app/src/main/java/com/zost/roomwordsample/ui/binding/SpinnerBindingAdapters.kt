package com.zost.roomwordsample.ui.binding

import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Spinner
import androidx.databinding.BindingAdapter
import androidx.databinding.InverseBindingAdapter
import androidx.databinding.InverseBindingListener
import com.zost.roomwordsample.R

@BindingAdapter("app:entries")
fun Spinner.setEntries(entries: List<Any>?) {
    entries?.let {
        val arrayAdapter = ArrayAdapter(context, R.layout.form_spinner_element_view, entries)
        arrayAdapter.setDropDownViewResource(R.layout.form_spinner_element_dropdown_item)
        adapter = arrayAdapter
    }
}

@BindingAdapter("selectedValueAttrChanged")
fun Spinner.setInverseBindingListener(listener: InverseBindingListener?) {
    if (listener == null) {
        onItemSelectedListener = null
    } else {
        onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                if (tag != position) {
                    listener.onChange()
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>) {}
        }
    }
}

@BindingAdapter("app:selectedValue")
fun Spinner.setNewValue(value: Any?) {
    adapter?.let {
        val position = (adapter as ArrayAdapter<Any>).getPosition(value)
        setSelection(position, false)
        tag = position
    }
}

@InverseBindingAdapter(attribute = "selectedValue", event = "selectedValueAttrChanged")
fun Spinner.getSelectedValue(): Any? = selectedItem