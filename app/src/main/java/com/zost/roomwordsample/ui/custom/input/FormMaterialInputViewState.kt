package com.zost.roomwordsample.ui.custom.input

import androidx.appcompat.widget.AppCompatEditText
import com.github.vivchar.rendererrecyclerviewadapter.ViewHolder
import com.thejuki.kformmaster.state.BaseFormViewState
import com.thejuki.kformmaster.R

class FormMaterialInputViewState(holder: ViewHolder) : BaseFormViewState(holder) {

    private var value: String? = null

    init {
        val editText = holder.viewFinder.find(R.id.formElementValue) as AppCompatEditText
        value = editText.text.toString()
    }

    override fun restore(holder: ViewHolder) {
        super.restore(holder)
        holder.viewFinder.setText(R.id.formElementValue, value)
    }

}