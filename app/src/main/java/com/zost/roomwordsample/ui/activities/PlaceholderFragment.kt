package com.zost.roomwordsample.ui.activities

import android.content.Context
import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.zost.roomwordsample.R

/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [PlaceholderFragment.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [PlaceholderFragment.newInstance] factory method to
 * create an instance of this fragment.
 *
 */
class PlaceholderFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_placeholder, container, false)
    }

}
