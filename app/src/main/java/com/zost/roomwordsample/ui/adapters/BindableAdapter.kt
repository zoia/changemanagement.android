package com.zost.roomwordsample.ui.adapters

interface BindableAdapter<T> {
    fun setData(data: List<T>)
    fun setItemClickedListener(listener: ItemClickedListener<T>)
}

interface ItemClickedListener<T> {
    fun onItemClicked(item: T)
}