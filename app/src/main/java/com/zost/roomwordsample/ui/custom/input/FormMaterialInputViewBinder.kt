package com.zost.roomwordsample.ui.custom.input

import android.content.Context
import android.text.InputType
import android.text.method.ScrollingMovementMethod
import android.view.View
import android.view.inputmethod.EditorInfo
import com.github.vivchar.rendererrecyclerviewadapter.ViewHolder
import com.github.vivchar.rendererrecyclerviewadapter.ViewState
import com.github.vivchar.rendererrecyclerviewadapter.ViewStateProvider
import com.github.vivchar.rendererrecyclerviewadapter.binder.ViewBinder
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout
import com.thejuki.kformmaster.helper.FormBuildHelper
import com.thejuki.kformmaster.view.BaseFormViewBinder
import com.zost.roomwordsample.R
import com.zost.roomwordsample.infrastructure.setRightDrawable
import com.zost.roomwordsample.infrastructure.onRightDrawableClicked

class FormMaterialInputViewBinder(
    private val context: Context,
    private val formBuilder: FormBuildHelper
) : BaseFormViewBinder() {

    var viewBinder =
        ViewBinder(R.layout.form_material_input_element, FormMaterialInputElement::class.java,
            { model, finder, payloads ->

                val mainViewLayout = finder.find(R.id.formElementMainLayout) as TextInputLayout
                val itemView = finder.getRootView() as View
                baseSetup(model, null, null, null, itemView, mainViewLayout)

                val editTextValue =
                    finder.find(R.id.formElementValue) as TextInputEditText

                editTextValue.setText(model.valueAsString)

                mainViewLayout.hint = model.floatingLabel ?: ""

                model.rightToLeft = false
                model.editView = editTextValue

                if (model.numbersOnly) {

                    editTextValue.inputType = InputType.TYPE_CLASS_NUMBER
                }

                if (model.isMultiline) {

                    editTextValue.setSingleLine(false)
                    editTextValue.inputType = InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_FLAG_MULTI_LINE
                    editTextValue.imeOptions = EditorInfo.IME_FLAG_NO_ENTER_ACTION
                    editTextValue.isVerticalScrollBarEnabled = true
                    editTextValue.movementMethod = ScrollingMovementMethod.getInstance()
                    editTextValue.scrollBarStyle = View.SCROLLBARS_INSIDE_INSET
                }

                if (model.required) {

                    model.valueObservers.add { _, element ->
                        model.performValidation()
                    }
                }

                if (model.clearable) {

                    if (!model.value.isNullOrEmpty()) {
                        editTextValue.setRightDrawable(R.drawable.ic_clear_black_24dp)
                    }

                    editTextValue.onRightDrawableClicked {
                        model.value = null
                    }

                    model.valueObservers.add { newValue, element ->
                        val input = element.editView as TextInputEditText
                        input.setRightDrawable(
                            if (newValue.isNullOrEmpty()) null
                            else R.drawable.ic_clear_black_24dp
                        )
                    }
                }

                setOnFocusChangeListener(context, model, formBuilder)
                addTextChangedListener(model, formBuilder)
                setOnEditorActionListener(model, formBuilder)

            }, object : ViewStateProvider<FormMaterialInputElement, ViewHolder> {
                override fun createViewStateID(model: FormMaterialInputElement): Int {
                    return model.id
                }

                override fun createViewState(holder: ViewHolder): ViewState<ViewHolder> {
                    return FormMaterialInputViewState(holder)
                }
            })

}