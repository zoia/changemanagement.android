package com.zost.roomwordsample.ui.custom.spinner

import android.content.res.ColorStateList
import android.widget.ArrayAdapter
import android.widget.Spinner
import android.widget.TextView
import com.thejuki.kformmaster.model.BaseFormElement
import com.zost.roomwordsample.R
import com.zost.roomwordsample.ui.activities.DropDownItem
import com.zost.roomwordsample.ui.custom.ValidatableAttribute

class FormSpinnerElement(override var attributeId: Int) :
    BaseFormElement<DropDownItem>(-1),
    ValidatableAttribute {

    override fun performValidation() : Boolean {

        val context = editView?.context!!

        val valid = validityCheck()
        error = if (valid) null else "Будь ласка, оберіть значення"
        val color = context.resources.getColor(
            if (valid) R.color.abc_hint_foreground_material_dark
            else R.color.design_error,
            context.theme
        )
        titleView?.setTextColor(color)

        val spinnerView = editView as Spinner
        spinnerView.backgroundTintList = ColorStateList.valueOf(color)

        val selectedView = spinnerView.selectedView as TextView
        selectedView.setTextColor(
            context.resources.getColor(
                if (valid) R.color.abc_primary_text_material_dark else R.color.design_error,
                context.theme
            )
        )

        return valid
    }

    override var validityCheck: () -> Boolean = {
        !required || value?.id != null
    }

    var values: List<DropDownItem> = ArrayList()
        set (value) {
            field = value
            val spinner = editView as? Spinner
            spinner?.let {
                val adapter = spinner.adapter as ArrayAdapter<DropDownItem>
                adapter.clear()
                adapter.addAll(field)
            }
        }

}