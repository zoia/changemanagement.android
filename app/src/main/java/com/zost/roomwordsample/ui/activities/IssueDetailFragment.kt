package com.zost.roomwordsample.ui.activities

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.zost.roomwordsample.R
import com.zost.roomwordsample.databinding.IssueDetailBinding
import com.zost.roomwordsample.ui.adapters.IssueAttributesAdapter
import com.zost.roomwordsample.viewmodels.IssueDetailsViewModel
import kotlinx.android.synthetic.main.issue_detail.view.*

class IssueDetailFragment : Fragment() {

    companion object {
        fun newInstance(issueId: Int, showComments: Boolean = false) =
            IssueDetailFragment().apply {
                arguments = Bundle().apply {
                    putInt(ARG_ITEM_ID, issueId)
                    putBoolean(SHOW_COMMENTS, showComments)
                }
            }

        const val ARG_ITEM_ID = "IssueId"

        const val ISSUE_SUMMARY = "summary"

        private const val SHOW_COMMENTS = "show_comments"
    }

    lateinit var viewModel: IssueDetailsViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(IssueDetailsViewModel::class.java)
        viewModel.showComments.value = arguments!!.getBoolean(SHOW_COMMENTS)

        val issueId = arguments!!.getInt(ARG_ITEM_ID)
        viewModel.loadIssue(issueId)
        viewModel.commentsRequested.observe(this, Observer { event ->
            event.getContentIfNotHandled()?.let {
                val intent = Intent(this@IssueDetailFragment.activity, IssueCommentsActivity::class.java).apply {
                    putExtra(ARG_ITEM_ID, issueId)
                    putExtra(ISSUE_SUMMARY, viewModel.summary.value)
                }
                startActivity(intent)
            }
        })
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val binding = DataBindingUtil.inflate<IssueDetailBinding>(
            inflater, R.layout.issue_detail, container, false
        ).apply {
            setLifecycleOwner(this@IssueDetailFragment)
            viewModel = this@IssueDetailFragment.viewModel
        }

        val layoutManager = LinearLayoutManager(activity)
        binding.root.attributesView.layoutManager = layoutManager

        val adapter = IssueAttributesAdapter(ArrayList())
        binding.root.attributesView.adapter = adapter

        return binding.root
    }
}
