package com.zost.roomwordsample.ui.binding

import androidx.databinding.BindingAdapter
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout

@BindingAdapter("app:validationError")
fun setError(input: TextInputEditText, error: String?) {

    val parent = input.parent.parent as? TextInputLayout

    if (error == null || error.isEmpty()) {
        if (parent != null) parent.error = null
        else input.error = null
    } else {
        if (parent != null) parent.error = error
        else input.error = error
    }
}