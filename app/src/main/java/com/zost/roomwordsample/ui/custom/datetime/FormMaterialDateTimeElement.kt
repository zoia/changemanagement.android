package com.zost.roomwordsample.ui.custom.datetime

import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.view.View
import android.widget.TextView
import androidx.appcompat.widget.AppCompatEditText
import com.google.android.material.textfield.TextInputLayout
import com.thejuki.kformmaster.helper.FormBuildHelper
import com.thejuki.kformmaster.model.FormPickerDateTimeElement
import com.thejuki.kformmaster.model.FormPickerElement
import com.zost.roomwordsample.ui.custom.ValidatableAttribute
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*

class FormMaterialDateTimeElement(override var attributeId: Int) :
    FormPickerElement<FormPickerDateTimeElement.DateTimeHolder>(-1),
    ValidatableAttribute {

    override fun performValidation(): Boolean {
        val wrapper = mainLayoutView as TextInputLayout
        val valid = validityCheck()
        wrapper.error = if (valid) null else "Будь ласка, заповніть це поле"
        return valid
    }

    private var formBuildHelper: FormBuildHelper? = null

    var dateFormat: DateFormat = SimpleDateFormat.getDateInstance()
        set(value) {
            field = value
            this.value = FormPickerDateTimeElement.DateTimeHolder(dateValue, dateFormat)
            reInitDialog()
        }

    var dateValue: Date? = null
        set(value) {
            field = value
            this.value = FormPickerDateTimeElement.DateTimeHolder(dateValue, dateFormat)
            reInitDialog()
        }

    var maximumDate: Date? = null
        set(value) {
            field = value
            reInitDialog()
        }

    var minimumDate: Date? = null
        set(value) {
            field = value
            reInitDialog()
        }

    override fun clear() {
        this.value?.useCurrentDate()
        (this.editView as? TextView)?.text = ""
        this.valueObservers.forEach { it(this.value, this) }
    }

    override var validityCheck = { !required || value?.getTime() != null }

    /**
     * Re-initializes the dialog
     * Should be called value changes by user
     */
    fun reInitDialog(formBuilder: FormBuildHelper? = null) {

        if (formBuilder != null) {
            this.formBuildHelper = formBuilder
        }

        val editTextView = this.editView as? AppCompatEditText

        if (editTextView?.context == null) {
            return
        }

        val datePickerDialog = DatePickerDialog(
            editTextView.context,
            dateDialogListener(editTextView),
            value?.year ?: 0,
            if ((value?.month ?: 0) == 0) 0 else (value?.month ?: 0) - 1,
            value?.dayOfMonth ?: 0
        )

        maximumDate?.let {
            datePickerDialog.datePicker.maxDate = it.time
        }

        minimumDate?.let {
            datePickerDialog.datePicker.minDate = it.time
        }

        // display the dialog on click
        val listener = View.OnClickListener {
            datePickerDialog.show()
        }

        itemView?.setOnClickListener(listener)
        editTextView.setOnClickListener(listener)
    }

    private fun dateDialogListener(editTextValue: AppCompatEditText): DatePickerDialog.OnDateSetListener {
        return DatePickerDialog.OnDateSetListener { _, year, monthOfYear, dayOfMonth ->
            // get current form element, existing value and new value

            with(value)
            {
                this?.year = year
                this?.month = monthOfYear + 1
                this?.dayOfMonth = dayOfMonth
            }

            // Now show time picker
            TimePickerDialog(
                editTextValue.context, timeDialogListener(editTextValue),
                value?.hourOfDay ?: 0,
                value?.minute ?: 0,
                false
            ).show()
        }
    }

    private fun timeDialogListener(editTextValue: AppCompatEditText): TimePickerDialog.OnTimeSetListener {
        return TimePickerDialog.OnTimeSetListener { _, hourOfDay, minute ->
            with(value)
            {
                this?.hourOfDay = hourOfDay
                this?.minute = minute
                this?.isEmptyDateTime = false
            }
            error = null
            this.formBuildHelper?.onValueChanged(this)
            valueObservers.forEach { it(value, this) }
            editTextValue.setText(valueAsString)
        }
    }
}