package com.zost.roomwordsample.ui.activities

import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.GravityCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.zost.roomwordsample.App
import com.zost.roomwordsample.R
import com.zost.roomwordsample.databinding.ActivityIssueListBinding
import com.zost.roomwordsample.databinding.NavHeaderBinding
import com.zost.roomwordsample.ui.adapters.IssueListAdapter
import com.zost.roomwordsample.viewmodels.IssueListViewModel
import kotlinx.android.synthetic.main.activity_issue_list.*
import kotlinx.android.synthetic.main.app_toolbar.*
import kotlinx.android.synthetic.main.item_list.*

class IssueListActivity : BaseActivity() {

    private var twoPane: Boolean = false
    private lateinit var viewModel: IssueListViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        initViewModel()
        setupBinding()
        setupToolbar()
        setupIssueList()
        setupNavigation()

        fab.setOnClickListener {

            val selectedId = viewModel.issueSelected.value;
            if (twoPane && selectedId != null) {
                goToIssueDetails(selectedId.peekContent())
            } else {
                createIssue()
            }
        }

        if (item_detail_container != null) {
            twoPane = true

            val fragment = PlaceholderFragment()
            supportFragmentManager.beginTransaction()
                .replace(R.id.item_detail_container, fragment)
                .commit()
        }
    }

    private fun initViewModel() {
        viewModel = ViewModelProviders.of(this).get(IssueListViewModel::class.java)
        viewModel.load()

        viewModel.issueSelected.observe(this, Observer { event ->
            event.getContentIfNotHandled()?.let {

                if (twoPane) {
                    val fragment = IssueDetailFragment().apply {
                        arguments = Bundle().apply {
                            putInt(IssueDetailFragment.ARG_ITEM_ID, it)
                        }
                    }
                    supportFragmentManager.beginTransaction()
                        .replace(R.id.item_detail_container, fragment)
                        .addToBackStack(null)
                        .commit()

                    fab.setImageDrawable(resources.getDrawable(R.drawable.ic_open_in_new_black_24dp, theme))
                } else {
                    goToIssueDetails(it)
                }

            }
        })
    }

    private fun goToIssueDetails(issueId: Int) {
        val intent = Intent(this, IssueDetailActivity::class.java)
        intent.putExtra(IssueDetailFragment.ARG_ITEM_ID, issueId)
        startActivity(intent)
    }

    private fun setupNavigation() {
        nav_view.setNavigationItemSelectedListener { menuItem ->
            when (menuItem.itemId) {
                R.id.nav_logout -> logout()
                R.id.nav_new_issue -> createIssue()
                else -> true
            }
        }
    }

    private fun setupIssueList() {
        val layoutManager = LinearLayoutManager(this)
        recycler_view.layoutManager = layoutManager

        val adapter = IssueListAdapter(ArrayList())
        recycler_view.adapter = adapter
    }

    private fun setupToolbar() {
        setSupportActionBar(app_toolbar)
        supportActionBar?.apply {
            setDisplayHomeAsUpEnabled(true)
            setHomeAsUpIndicator(R.drawable.ic_menu_white_24dp)
            title = "Мої пости"
        }
    }

    private fun setupBinding() {
        val binding: ActivityIssueListBinding =
            DataBindingUtil.setContentView(this, R.layout.activity_issue_list)
        binding.setLifecycleOwner(this)
        binding.viewModel = viewModel

        val navHeaderBinding: NavHeaderBinding = DataBindingUtil.inflate(
            layoutInflater,
            R.layout.nav_header,
            binding.navView,
            false
        )
        navHeaderBinding.user = viewModel.user
        binding.navView.addHeaderView(navHeaderBinding.root)
    }

    private fun createIssue(): Boolean {
        startActivity(Intent(this, CreateIssueActivity::class.java))
        drawer_layout.closeDrawers()
        return true
    }

    private fun logout(): Boolean {
        App.preferences.accessToken = null
        App.preferences.refreshToken = null

        val intent = Intent(this, LoginActivity::class.java)
        finishAffinity()
        startActivity(intent)
        return true
    }

    override fun onRestart() {
        super.onRestart()
        viewModel.load()
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return when (item?.itemId) {
            android.R.id.home -> {
                drawer_layout.openDrawer(GravityCompat.START)
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

}
