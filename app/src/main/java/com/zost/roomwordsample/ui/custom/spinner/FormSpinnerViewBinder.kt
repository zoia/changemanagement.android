package com.zost.roomwordsample.ui.custom.spinner

import android.content.Context
import android.view.View
import android.widget.*
import androidx.appcompat.widget.AppCompatTextView
import com.github.vivchar.rendererrecyclerviewadapter.ViewHolder
import com.github.vivchar.rendererrecyclerviewadapter.ViewState
import com.github.vivchar.rendererrecyclerviewadapter.ViewStateProvider
import com.github.vivchar.rendererrecyclerviewadapter.binder.ViewBinder
import com.thejuki.kformmaster.extensions.setMargins
import com.thejuki.kformmaster.helper.FormBuildHelper
import com.thejuki.kformmaster.view.BaseFormViewBinder
import com.zost.roomwordsample.R
import com.zost.roomwordsample.ui.activities.DropDownItem
import com.zost.roomwordsample.ui.adapters.DropDownItemArrayAdapter


class FormSpinnerViewBinder(
    private val context: Context,
    private val formBuilder: FormBuildHelper
) : BaseFormViewBinder() {

    var viewBinder =
        ViewBinder(
            R.layout.form_spinner_element, FormSpinnerElement::class.java,
            { model, finder, _ ->

                val textViewTitle = finder.find(R.id.formElementTitle) as AppCompatTextView
                val mainViewLayout = finder.find(R.id.formElementMainLayout) as LinearLayout

                val error = finder.find(R.id.formElementError) as AppCompatTextView
                val itemView = finder.getRootView() as View
                baseSetup(model, null, textViewTitle, error, itemView, mainViewLayout)

                model.value = model.values.first()

                val spinner =
                    finder.find(R.id.formElementValue) as Spinner

                val adapter = DropDownItemArrayAdapter(
                    context,
                    R.layout.form_spinner_element_view,
                    model.values,
                    model.required
                )
                adapter.setDropDownViewResource(R.layout.form_spinner_element_dropdown_item)
                spinner.adapter = adapter
                spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {

                    private var isInitialized: Boolean = false

                    override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {

                        if (!isInitialized) {
                            isInitialized = true
                            return
                        }

                        val newValue = parent.selectedItem as? DropDownItem
                        model.setValue(newValue)
                    }

                    override fun onNothingSelected(parent: AdapterView<*>) {}
                }

                model.editView = spinner

                if (model.required) {
                    model.valueObservers.add { newValue, element ->
                        model.performValidation()
                    }
                }

            }, object : ViewStateProvider<FormSpinnerElement, ViewHolder> {
                override fun createViewStateID(model: FormSpinnerElement): Int {
                    return model.id
                }

                override fun createViewState(holder: ViewHolder): ViewState<ViewHolder> {
                    return FormSpinnerViewState(holder)
                }
            })
}