package com.zost.roomwordsample.ui.binding

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions

@BindingAdapter("app:imageUrl")
fun setImageFromUrl(imageView: ImageView, url: String?) {
    if (url == null) {
        return
    }

    Glide.with(imageView.context)
        .load(url)
        .apply(RequestOptions.circleCropTransform())
        .into(imageView)
}