package com.zost.roomwordsample.ui.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import com.zost.roomwordsample.ui.activities.DropDownItem

class DropDownItemArrayAdapter(
    context: Context,
    resource: Int,
    items: List<DropDownItem>,
    private val hideNullOption: Boolean
) : ArrayAdapter<DropDownItem>(context, resource, items) {

    override fun getDropDownView(position: Int, convertView: View?, parent: ViewGroup): View {

        val view = super.getDropDownView(position, convertView, parent)
        val current = super.getItem(position)
        if (hideNullOption && current?.id == null) {
            view.visibility = View.GONE
            return view
        }

        view.visibility = View.VISIBLE
        return view
    }

}