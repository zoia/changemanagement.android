package com.zost.roomwordsample.ui.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.zost.roomwordsample.R
import com.zost.roomwordsample.databinding.CommentItemLayoutBinding
import com.zost.roomwordsample.models.Comment

class CommentsAdapter(private var comments: List<Comment> = emptyList()) :
    RecyclerView.Adapter<CommentsAdapter.ViewHolder>(),
    BindableAdapter<Comment> {

    override fun setItemClickedListener(listener: ItemClickedListener<Comment>) {}

    override fun setData(data: List<Comment>) {
        comments = data
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int = comments.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = DataBindingUtil.inflate<CommentItemLayoutBinding>(
            LayoutInflater.from(parent.context),
            R.layout.comment_item_layout,
            parent,
            false
        )

        return ViewHolder(binding.root)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.binding?.apply {
            viewModel = comments[position]
        }
    }

    inner class ViewHolder(itemView: View) :
        RecyclerView.ViewHolder(itemView) {

        var binding: CommentItemLayoutBinding? = null

        init {
            binding = DataBindingUtil.bind(itemView)
        }

    }
}