package com.zost.roomwordsample.ui.custom

interface ValidatableAttribute {
    var attributeId: Int
    fun performValidation(): Boolean
}
