package com.zost.roomwordsample.ui.activities

import android.os.Bundle
import android.view.MenuItem
import android.widget.Button
import android.widget.EditText
import androidx.appcompat.app.AlertDialog
import com.zost.roomwordsample.R
import kotlinx.android.synthetic.main.activity_issue_comments.*

class IssueCommentsActivity : BaseActivity() {

    companion object {
        const val COMMENT_UPDATE = "comment_update"
    }

    private var issueId: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_issue_comments)

        setSupportActionBar(app_comments_toolbar)
        supportActionBar?.apply {
            setDisplayHomeAsUpEnabled(true)
            title = intent.getStringExtra(IssueDetailFragment.ISSUE_SUMMARY)
        }

        issueId = intent.getIntExtra(IssueDetailFragment.ARG_ITEM_ID, 0)
        val fragment = CommentSectionFragment.newInstance(issueId)
        supportFragmentManager.beginTransaction()
            .add(R.id.comments_container, fragment)
            .commit()

        fab_add_comment.setOnClickListener {
            val dialog = AlertDialog.Builder(this@IssueCommentsActivity)
                .create()
            val dialogView = layoutInflater.inflate(R.layout.dialog_add_comment, null)

            val okBtn = dialogView.findViewById<Button>(R.id.buttonSubmit)
            okBtn.setOnClickListener {
                fragment.viewModel.addComment(
                    issueId,
                    dialogView.findViewById<EditText>(R.id.edt_comment).text.toString()
                )
                dialog.dismiss() }

            val cancelBtn = dialogView.findViewById<Button>(R.id.buttonCancel)
            cancelBtn.setOnClickListener { dialog.dismiss() }

            dialog.setView(dialogView)
            dialog.show()
        }

    }

    override fun onOptionsItemSelected(item: MenuItem) =
        when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }

}
