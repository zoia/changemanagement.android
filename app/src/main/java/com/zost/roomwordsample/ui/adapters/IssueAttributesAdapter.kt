package com.zost.roomwordsample.ui.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.zost.roomwordsample.R
import com.zost.roomwordsample.databinding.IssueAttributeLayoutBinding
import com.zost.roomwordsample.models.IssueAttributeValueViewModel

class IssueAttributesAdapter(private var attributes: List<IssueAttributeValueViewModel> = emptyList()) :
    RecyclerView.Adapter<IssueAttributesAdapter.ViewHolder>(),
    BindableAdapter<IssueAttributeValueViewModel> {

    private var clickedListener: ItemClickedListener<IssueAttributeValueViewModel>? = null

    override fun setData(data: List<IssueAttributeValueViewModel>) {
        attributes = data
        notifyDataSetChanged()
    }

    override fun setItemClickedListener(listener: ItemClickedListener<IssueAttributeValueViewModel>) {
        clickedListener = listener
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        return attributes.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = DataBindingUtil.inflate<IssueAttributeLayoutBinding>(
            LayoutInflater.from(parent.context),
            R.layout.issue_attribute_layout,
            parent,
            false
        )

        return ViewHolder(binding.root)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.binding?.apply {
            viewModel = attributes[position]
        }
    }

    inner class ViewHolder(itemView: View) :
        RecyclerView.ViewHolder(itemView) {

        var binding: IssueAttributeLayoutBinding? = null

        init {
            binding = DataBindingUtil.bind(itemView)
        }
    }

}