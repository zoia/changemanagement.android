package com.zost.roomwordsample.ui.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.zost.roomwordsample.R
import com.zost.roomwordsample.databinding.IssueItemLayoutBinding
import com.zost.roomwordsample.models.IssuePreview

class IssueListAdapter(private var issues: List<IssuePreview> = emptyList()) :
    RecyclerView.Adapter<IssueListAdapter.ViewHolder>(),
    BindableAdapter<IssuePreview> {

    private var clickedListener: ItemClickedListener<IssuePreview>? = null

    override fun setData(data: List<IssuePreview>) {
        issues = data
        notifyDataSetChanged()
    }

    override fun setItemClickedListener(listener: ItemClickedListener<IssuePreview>) {
        clickedListener = listener
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int = issues.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = DataBindingUtil.inflate<IssueItemLayoutBinding>(
            LayoutInflater.from(parent.context),
            R.layout.issue_item_layout,
            parent,
            false
        )

        return ViewHolder(binding.root)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.binding?.apply {
            viewModel = issues[position]
            itemClickListener = clickedListener
        }
    }

    inner class ViewHolder(itemView: View) :
        RecyclerView.ViewHolder(itemView) {

        var binding: IssueItemLayoutBinding? = null

        init {
            binding = DataBindingUtil.bind(itemView)
        }
    }

}