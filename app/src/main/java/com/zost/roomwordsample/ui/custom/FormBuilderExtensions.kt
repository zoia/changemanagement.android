package com.zost.roomwordsample.ui.custom

import com.thejuki.kformmaster.helper.FormBuildHelper
import com.zost.roomwordsample.ui.custom.datetime.FormMaterialDateTimeElement
import com.zost.roomwordsample.ui.custom.input.FormMaterialInputElement
import com.zost.roomwordsample.ui.custom.spinner.FormSpinnerElement

fun FormBuildHelper.materialInput(
    attributeId: Int,
    init: FormMaterialInputElement.() -> Unit
): FormMaterialInputElement = addFormElement(FormMaterialInputElement(attributeId).apply(init))

fun FormBuildHelper.spinner(
    attributeId: Int,
    init: FormSpinnerElement.() -> Unit
): FormSpinnerElement {
    return addFormElement(FormSpinnerElement(attributeId).apply(init))
}

fun FormBuildHelper.materialDateTime(
    attributeId: Int,
    init: FormMaterialDateTimeElement.() -> Unit
): FormMaterialDateTimeElement {
    return addFormElement(FormMaterialDateTimeElement(attributeId).apply(init))
}