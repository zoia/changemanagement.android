package com.zost.roomwordsample.ui.activities

import android.content.Intent
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import arrow.data.*
import com.zost.roomwordsample.R
import com.zost.roomwordsample.databinding.ActivityLoginBinding
import com.zost.roomwordsample.infrastructure.Event
import com.zost.roomwordsample.infrastructure.validateRequired
import com.zost.roomwordsample.viewmodels.LoginViewModel

data class DropDownItem(val id: Int?, val data: String) {
    override fun toString(): String = data
}

class LoginActivity : BaseActivity() {

    private lateinit var loginViewModel: LoginViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        loginViewModel = ViewModelProviders.of(this).get(LoginViewModel::class.java)

        loginViewModel.successfullyAuthenticated.observe(this, Observer<Event<Boolean>> {
            it?.getContentIfNotHandled()?.let {
                startActivity(Intent(this, IssueListActivity::class.java))
            }
        })

        val binding: ActivityLoginBinding =
            DataBindingUtil.setContentView(this, R.layout.activity_login)
        binding.setLifecycleOwner(this)
        binding.viewModel = loginViewModel
    }
}
