package com.zost.roomwordsample.ui.custom.datetime

import android.content.Context
import android.text.InputType
import android.view.View
import com.github.vivchar.rendererrecyclerviewadapter.ViewHolder
import com.github.vivchar.rendererrecyclerviewadapter.ViewState
import com.github.vivchar.rendererrecyclerviewadapter.ViewStateProvider
import com.github.vivchar.rendererrecyclerviewadapter.binder.ViewBinder
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout
import com.thejuki.kformmaster.helper.FormBuildHelper
import com.thejuki.kformmaster.model.FormPickerDateTimeElement
import com.thejuki.kformmaster.state.FormEditTextViewState
import com.thejuki.kformmaster.view.BaseFormViewBinder
import com.zost.roomwordsample.R
import com.zost.roomwordsample.infrastructure.onRightDrawableClicked
import com.zost.roomwordsample.infrastructure.setRightDrawable

class FormMaterialDateTimeViewBinder(
    private val context: Context,
    private val formBuilder: FormBuildHelper
) : BaseFormViewBinder() {

    val viewBinder = ViewBinder(R.layout.form_material_input_element,
        FormMaterialDateTimeElement::class.java,
        { model, finder, _ ->

            val mainViewLayout = finder.find(R.id.formElementMainLayout) as TextInputLayout
            val itemView = finder.getRootView() as View
            baseSetup(model, null, null, null, itemView, mainViewLayout)

            val editTextValue = finder.find(R.id.formElementValue) as TextInputEditText

            editTextValue.setText(model.valueAsString)
            mainViewLayout.hint = model.hint ?: ""

            model.editView = editTextValue

            editTextValue.setRawInputType(InputType.TYPE_NULL)
            editTextValue.isFocusable = false

            // If no value is set by the user, create a new instance of DateTimeHolder
            with(model.value)
            {
                if (this == null) {
                    model.setValue(FormPickerDateTimeElement.DateTimeHolder())
                }
                this?.validOrCurrentDate()
            }

            model.reInitDialog(formBuilder)

            if (model.required) {

                model.valueObservers.add { newValue, element ->
                    model.performValidation()
                }
            }

            if (model.clearable) {

                if (model.value?.getTime() != null) {
                    editTextValue.setRightDrawable(R.drawable.ic_clear_black_24dp)
                }

                editTextValue.onRightDrawableClicked {
                    model.clear()
                }

                model.valueObservers.add { newValue, element ->
                    val input = element.editView as TextInputEditText
                    input.setRightDrawable(
                        if (newValue?.getTime() == null) null
                        else R.drawable.ic_clear_black_24dp
                    )
                }
            }
        }, object : ViewStateProvider<FormMaterialDateTimeElement, ViewHolder> {
            override fun createViewStateID(model: FormMaterialDateTimeElement): Int {
                return model.id
            }

            override fun createViewState(holder: ViewHolder): ViewState<ViewHolder> {
                return FormEditTextViewState(holder)
            }
        })
}
