package com.zost.roomwordsample.ui.custom.input

import android.text.InputType
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout
import com.thejuki.kformmaster.model.BaseFormElement
import com.zost.roomwordsample.ui.custom.ValidatableAttribute

class FormMaterialInputElement(override var attributeId: Int) :
    BaseFormElement<String>(-1),
    ValidatableAttribute {

    override fun performValidation() : Boolean {
        val wrapper = mainLayoutView as TextInputLayout
        val valid = validityCheck()
        wrapper.error = if (valid) null else "Будь ласка, заповніть це поле"
        return valid
    }

    var floatingLabel: String? = null
        set (value) {
            field = value
            val layout = mainLayoutView as? TextInputLayout
            layout?.hint = value
        }

    var isMultiline: Boolean = false

    var numbersOnly: Boolean = false
        set(value) {
            field = value
            val input = itemView as? TextInputEditText
            input?.let {
                if (numbersOnly) {
                    input.inputType = InputType.TYPE_CLASS_NUMBER
                } else {
                    input.setRawInputType(InputType.TYPE_CLASS_NUMBER or InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS)
                }
            }
        }

}