package com.zost.roomwordsample.infrastructure.communication

import okhttp3.Response
import retrofit2.HttpException

sealed class Result<out T : Any> {

    class Ok<out T : Any>(val value: T, override val response: Response) : Result<T>(),
        ResponseResult {
        override fun toString() = "Result.OK{value=$value, response=$response}"
    }

    class Error(
        override val exception: HttpException,
        override val response: Response
    ) : Result<Nothing>(),
        ErrorResult,
        ResponseResult {
        override fun toString() = "Result.Error{exception=$exception}"
    }

    class Exception(override val exception: Throwable) : Result<Nothing>(),
        ErrorResult {
        override fun toString() = "Result.Exception{exception=$exception}"
    }

    fun getOrNull(): T? = (this as? Ok)?.value

    fun getOrThrow(throwable: Throwable? = null): T {
        return when (this) {
            is Ok -> value
            is Error -> throw throwable ?: exception
            is Exception -> throw throwable ?: exception
        }
    }
}

interface ResponseResult {
    val response: Response
}

interface ErrorResult {
    val exception: Throwable
}
