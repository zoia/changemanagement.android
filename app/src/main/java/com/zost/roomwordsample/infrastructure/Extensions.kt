package com.zost.roomwordsample.infrastructure

import android.util.Base64
import android.view.MotionEvent
import android.widget.EditText
import androidx.annotation.DrawableRes
import androidx.core.content.ContextCompat
import androidx.lifecycle.MutableLiveData
import arrow.data.*
import com.google.gson.Gson
import com.google.gson.JsonParser
import com.zost.roomwordsample.infrastructure.communication.Result
import com.zost.roomwordsample.models.TokenUser
import kotlinx.coroutines.*
import retrofit2.HttpException
import retrofit2.Response
import kotlin.coroutines.resume

fun String.decodeToken(): TokenUser {
    val parts = split('.')
    if (parts.size != 3) {
        throw IllegalArgumentException("JWT must have 3 parts.")
    }

    val decodedBytes = Base64.decode(parts[1], Base64.URL_SAFE)
    val data = String(decodedBytes)
    val user = Gson().fromJson(data, TokenUser::class.java)

    val parser = JsonParser()
    val jsonObject = parser.parse(data).asJsonObject
    val configuration = jsonObject.get("configuration")?.asString
    configuration?.let {
        user.defaultProjectId = parser.parse(configuration)
            .asJsonObject.get("defaultProject")?.asInt
    }

    return user
}

fun <T : Any?> MutableLiveData<T>.default(initialValue: T) = apply { setValue(initialValue) }

fun <T : Any> Validated<Nel<String>, T>.getOrThrow() = fold({ throw Exception("Value is not valid!") }) { it }

suspend fun <T : Any> Deferred<Response<T>>.awaitResult(): Result<T> {
    return suspendCancellableCoroutine { continuation ->
        GlobalScope.launch {
            try {
                val response = await()
                continuation.resume(
                    if (response.isSuccessful) {
                        val body = response.body()
                        body?.let {
                            Result.Ok(it, response.raw())
                        } ?: "error".let {
                            if (response.code() == 200) {
                                Result.Exception(Exception("body is empty"))
                            } else {
                                Result.Exception(
                                    NullPointerException("Response body is null")
                                )
                            }
                        }
                    } else {
                        Result.Error(
                            HttpException(response),
                            response.raw()
                        )
                    }
                )
            } catch (e: Throwable) {
                continuation.resume(Result.Exception(e))
            }
        }

        registerOnCompletion(continuation)
    }
}

private fun Deferred<Response<*>>.registerOnCompletion(continuation: CancellableContinuation<*>) {
    continuation.invokeOnCancellation {
        if (continuation.isCancelled) {
            try {
                cancel()
            } catch (ex: Throwable) {
                ex.printStackTrace()
            }
        }
    }
}

fun EditText.onRightDrawableClicked(onClicked: (view: EditText) -> Unit) {

    this.setOnTouchListener { v, event ->
        var hasConsumed = false
        if (v is EditText) {
            if (event.x >= v.width - v.totalPaddingRight) {
                if (event.action == MotionEvent.ACTION_UP) {
                    onClicked(this)
                }
                hasConsumed = true
            }
        }
        hasConsumed
    }
}

fun EditText.setRightDrawable(@DrawableRes id: Int?) {
    if (id != null) {
        val clear = ContextCompat.getDrawable(context, id)
        clear?.setBounds(0, 0, clear.intrinsicWidth, clear.intrinsicHeight)
        this.setCompoundDrawables(null, null, clear, null)
    } else {
        this.setCompoundDrawables(null, null, null, null)
    }
}

fun String?.validateRequired(errorMessage: String): Validated<Nel<String>, String> =
    when {
        this != null && !this.isEmpty() -> this.valid()
        else -> errorMessage.nel().invalid()
    }