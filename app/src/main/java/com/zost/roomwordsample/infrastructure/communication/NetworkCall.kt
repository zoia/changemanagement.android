package com.zost.roomwordsample.infrastructure.communication

import com.google.gson.JsonParser
import com.zost.roomwordsample.infrastructure.awaitResult
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Deferred
import kotlinx.coroutines.withContext
import retrofit2.HttpException
import retrofit2.Response
import java.net.ConnectException
import java.net.SocketTimeoutException

class CallHandler<TResponse : Any, TData : Any>(private val callScope: CoroutineScope) {
    lateinit var client: Deferred<Response<TResponse>>

    suspend fun mapResponse(): Resource<TData> {

        return withContext(callScope.coroutineContext) {
            try {
                val rawResponse = client.awaitResult().getOrThrow()
                val data = rawResponse as? TData
                data?.let {
                    return@withContext Resource.success(it)
                }

                val response = rawResponse as? DataContainer<TData>
                val unwrapped = response?.let {
                    Resource.success(it.retrieveData())
                } ?: Resource.error("Дані, отримані з серверу не можуть бути конвертовані.", null)

                unwrapped

            } catch (e: HttpException) {
                e.printStackTrace()

                if (e.code() == 400) {

                    val errorMessage = e.response().errorBody()?.string()
                    val message = JsonParser().parse(errorMessage).asJsonObject["error_description"].asString

                    Resource.error(message ?: "${e.message} | code ${e.response().code()}", null)
                }

                Resource.error(e.message(), null)

            } catch (e: SocketTimeoutException) {
                e.printStackTrace()
                Resource.error("Час очікування вичерпався!\n${e.message}", null)

            } catch (e: ConnectException) {
                e.printStackTrace()
                Resource.error("Не вдалось з'єднатись з сервером!\n${e.message}", null)
            }
        }
    }
}

interface DataContainer<T> {
    fun retrieveData(): T
}

suspend fun <TResponse : Any, TData : Any> call(
    scope: CoroutineScope,
    block: CallHandler<TResponse, TData>.() -> Unit
): Resource<TData> = CallHandler<TResponse, TData>(scope).apply(block).mapResponse()
