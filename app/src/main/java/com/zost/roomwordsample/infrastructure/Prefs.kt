package com.zost.roomwordsample.infrastructure

import android.content.Context
import android.content.SharedPreferences
import com.zost.roomwordsample.models.TokenUser

class Prefs(context: Context) {

    companion object {
        private const val PREFS_FILENAME = "com.zost.changemanagement.prefs"
        private const val ACCESS_TOKEN = "$PREFS_FILENAME.access_token"
        private const val REFRESH_TOKEN = "$PREFS_FILENAME.refresh_token"
        private const val DEFAULT_PROJECT_ID = "$PREFS_FILENAME.default_project_id"
        private const val FULL_NAME = "$PREFS_FILENAME.full_name"
        private const val EMAIL = "$PREFS_FILENAME.email"
        private const val PHONE_NUMBER = "$PREFS_FILENAME.phone_number"
        private const val JOB_TITLE = "$PREFS_FILENAME.job_title"
        private const val ID = "$PREFS_FILENAME.id"
    }

    private val prefs: SharedPreferences = context.getSharedPreferences(PREFS_FILENAME, Context.MODE_PRIVATE)

    var accessToken: String?
        get() = prefs.getString(ACCESS_TOKEN, null)
        set(value) = prefs.edit().putString(ACCESS_TOKEN, value).apply()

    var refreshToken: String?
        get() = prefs.getString(REFRESH_TOKEN, null)
        set(value) = prefs.edit().putString(REFRESH_TOKEN, value).apply()

    var defaultProjectId: Int
        get() = prefs.getInt(DEFAULT_PROJECT_ID, 0)
        set(value) = prefs.edit().putInt(DEFAULT_PROJECT_ID, value).apply()

    var fullName: String
        get() = prefs.getString(FULL_NAME, "")
        set(value) = prefs.edit().putString(FULL_NAME, value).apply()

    var email: String
        get() = prefs.getString(EMAIL, "")
        set(value) = prefs.edit().putString(EMAIL, value).apply()

    var phoneNumber: String
        get() = prefs.getString(PHONE_NUMBER, "")
        set(value) = prefs.edit().putString(PHONE_NUMBER, value).apply()

    var jobTitle: String
        get() = prefs.getString(JOB_TITLE, "")
        set(value) = prefs.edit().putString(JOB_TITLE, value).apply()

    var id: String
        get() = prefs.getString(ID, "")
        set(value) = prefs.edit().putString(ID, value).apply()

    fun setUser(user: TokenUser) {
        fullName = user.fullName
        email = user.email
        phoneNumber = user.phone
        jobTitle = user.jobTitle
        id = user.id

        user.defaultProjectId?.let {
            defaultProjectId = it
        }
    }
}