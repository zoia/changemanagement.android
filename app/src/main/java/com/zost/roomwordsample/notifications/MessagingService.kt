package com.zost.roomwordsample.notifications

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.media.RingtoneManager
import android.os.Build
import android.util.Log
import androidx.core.app.NotificationCompat
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.bumptech.glide.Glide
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.zost.roomwordsample.R
import com.zost.roomwordsample.ui.activities.CommentSectionFragment
import com.zost.roomwordsample.ui.activities.IssueCommentsActivity
import com.zost.roomwordsample.ui.activities.IssueDetailActivity
import com.zost.roomwordsample.ui.activities.IssueDetailFragment


class MessagingService : FirebaseMessagingService() {

    companion object {
        const val NOTIFICATION_CHANNEL = "MyNotificationChannel"
    }

    override fun onNewToken(token: String?) {
        super.onNewToken(token)
        Log.d("TOKEN_CMT", token)
    }

    override fun onMessageReceived(message: RemoteMessage?) {

        if (message == null) {
            return
        }

        val isComment = message.data["Comment"] != null
        if (isComment) {
            handleComment(message)
        } else {
            handleStatus(message)
        }

    }

    private fun handleComment(message: RemoteMessage) {
        val commentText = message.data["Comment"]!!
        val issueId = message.data[IssueDetailFragment.ARG_ITEM_ID]!!.toInt()
        val userProfilePicture = message.data["UserProfilePicture"]
        val commentId = message.data["CommentId"]!!.toInt()
        val userFullName = message.data["UserFullName"]!!
        val summary = message.data["IssueSummary"]!!

        val intent = Intent(applicationContext, IssueCommentsActivity::class.java)
        intent.putExtra(IssueDetailFragment.ARG_ITEM_ID, issueId)
        intent.putExtra(IssueDetailFragment.ISSUE_SUMMARY, summary)

        showMessage(summary, "$userFullName: $commentText", intent, "$issueId-$commentId", userProfilePicture)

        LocalBroadcastManager.getInstance(this)
            .sendBroadcast(
                CommentSectionFragment.getCommentUpdateIntent(
                    commentId,
                    commentText,
                    userFullName,
                    userProfilePicture
                )
            )
    }

    private fun handleStatus(message: RemoteMessage) {
        val title = message.data["Title"]
        val body = message.data["Body"]
        val issueId = message.data[IssueDetailFragment.ARG_ITEM_ID]!!.toInt()

        val intent = Intent(applicationContext, IssueDetailActivity::class.java)
        intent.putExtra(IssueDetailFragment.ARG_ITEM_ID, issueId)

        showMessage(title, body, intent, issueId.toString())
    }

    private fun showMessage(
        title: String?,
        body: String?,
        intent: Intent,
        notificationId: String,
        imageUrl: String? = null
    ) {
        val pendingIntent = PendingIntent.getActivity(
            applicationContext, 0, intent,
            PendingIntent.FLAG_ONE_SHOT
        )

        val soundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
        val notificationBuilder = NotificationCompat.Builder(this, NOTIFICATION_CHANNEL)
            .setSmallIcon(R.mipmap.ic_launcher)
            .setContentTitle(title)
            .setStyle(NotificationCompat.BigTextStyle().bigText(body))
            .setAutoCancel(true)
            .setSound(soundUri)
            .setContentIntent(pendingIntent)

        imageUrl?.let {
            val futureTarget = Glide.with(this)
                .asBitmap()
                .load(it)
                .submit()

            val bitmap = futureTarget.get()
            notificationBuilder.setLargeIcon(bitmap)

            Glide.with(this)
                .clear(futureTarget)
        }

        val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel =
                NotificationChannel(NOTIFICATION_CHANNEL, "Default channel", NotificationManager.IMPORTANCE_DEFAULT)
            notificationManager.createNotificationChannel(channel)
        }

        notificationManager.notify(notificationId.hashCode(), notificationBuilder.build())
    }

}