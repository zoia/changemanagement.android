package com.zost.roomwordsample.http

import com.zost.roomwordsample.App
import com.zost.roomwordsample.infrastructure.awaitResult
import com.zost.roomwordsample.models.TokenResponse
import kotlinx.coroutines.runBlocking
import okhttp3.*

class TokenAuthenticator : Authenticator {

    override fun authenticate(route: Route?, response: Response): Request? {

        var refreshToken: String = App.preferences.refreshToken ?: return null

        var refreshResponse: TokenResponse? = null
        runBlocking {
            refreshResponse = Api.authService.refreshToken(refreshToken).awaitResult().getOrNull()
        }

        val token = refreshResponse ?: return null

        App.preferences.accessToken = token.accessToken
        App.preferences.refreshToken = token.refreshToken

        return response.request().newBuilder()
            .header("Authorization", "Bearer ${App.preferences.accessToken}")
            .build()
    }

}