package com.zost.roomwordsample.http

import com.google.gson.GsonBuilder
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import com.zost.roomwordsample.http.interceptors.HeaderInterceptor
import com.zost.roomwordsample.http.services.*
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.*
import java.util.concurrent.TimeUnit

object Api {

    private const val BASE_HOST = "https://changemanagementweb.azurewebsites.net"
    //private const val BASE_HOST = "http://192.168.0.101:5001"

    private const val BASE_URL = BASE_HOST

    private const val WEB_SITE_URL = BASE_HOST

    const val ISSUE_DETAILS_URL = "$WEB_SITE_URL/issues/details/"

    private val httpClient: OkHttpClient = OkHttpClient.Builder()
        .connectTimeout(60, TimeUnit.SECONDS)
        .readTimeout(60, TimeUnit.SECONDS)
        .addInterceptor(HeaderInterceptor())
        .authenticator(TokenAuthenticator())
        .build()

    private val builder: Retrofit.Builder = Retrofit.Builder()
        .baseUrl(BASE_URL)
        .addConverterFactory(
            GsonConverterFactory.create(
                GsonBuilder().registerTypeAdapter(
                    Date::class.java,
                    DateDeserializer()
                ).create()
            )
        )
        .addCallAdapterFactory(CoroutineCallAdapterFactory())

    private val retrofit = builder
        .client(httpClient)
        .build()

    val issueService = retrofit.create<IssueService>(IssueService::class.java)

    val authService = retrofit.create<AuthService>(AuthService::class.java)

    val issueTypeService = retrofit.create<IssueTypeService>(IssueTypeService::class.java)

    val projectService = retrofit.create<ProjectService>(ProjectService::class.java)

    val commentsService = retrofit.create<CommentsService>(CommentsService::class.java)
}
