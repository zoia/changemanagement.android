package com.zost.roomwordsample.http.services

import com.zost.roomwordsample.models.Comment
import kotlinx.coroutines.Deferred
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Path

interface CommentsService {

    @GET("api/issues/{issueId}/comments")
    fun getComments(@Path("issueId") issueId: Int): Deferred<Response<List<Comment>>>

    @POST("api/issues/{issueId}/comments")
    fun addComment(@Path("issueId") issueId: Int, @Body comment: String): Deferred<Response<Comment>>

}