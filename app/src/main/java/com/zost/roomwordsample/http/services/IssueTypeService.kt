package com.zost.roomwordsample.http.services

import com.zost.roomwordsample.models.IssueType
import com.zost.roomwordsample.models.IssueTypeAttribute
import com.zost.roomwordsample.models.ListResult
import kotlinx.coroutines.Deferred
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface IssueTypeService {

    @GET("api/issueTypes")
    fun getIssueTypes(
        @Query("searchTerm") search: String? = null,
        @Query("page") page: Int? = null,
        @Query("pageSize") pageSize: Int? = null
    ): Deferred<Response<ListResult<IssueType>>>

    @GET("api/issueTypes/{issueTypeId}/attributes")
    fun getIssueTypesAttributes(
        @Path("issueTypeId") issueTypeId: Int
    ): Deferred<Response<List<IssueTypeAttribute>>>

}