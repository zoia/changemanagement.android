package com.zost.roomwordsample.http.services

import com.zost.roomwordsample.models.TokenResponse
import kotlinx.coroutines.Deferred
import retrofit2.Response
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST

interface AuthService {

    @FormUrlEncoded
    @POST("/connect/token")
    fun login(
        @Field("username") username: String,
        @Field("password") password: String,
        @Field("grant_type") grantType: String = "password",
        @Field("scope") scope: String = "openid email phone profile offline_access roles"
    ): Deferred<Response<TokenResponse>>

    @FormUrlEncoded
    @POST("/connect/token")
    fun refreshToken(
        @Field("refresh_token") refreshToken: String,
        @Field("grant_type") grantType: String = "refresh_token",
        @Field("scope") scope: String = "openid email phone profile offline_access roles"
    ): Deferred<Response<TokenResponse>>

}
