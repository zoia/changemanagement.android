package com.zost.roomwordsample.http.services

import com.zost.roomwordsample.models.Issue
import com.zost.roomwordsample.models.IssuePreview
import com.zost.roomwordsample.models.RawIssue
import kotlinx.coroutines.Deferred
import retrofit2.Response
import retrofit2.http.*

interface IssueService {

    @GET("api/issues/my")
    fun getIssueList(
        @Query("projectId") page: Int? = null,
        @Query("search") search: String? = null
    ): Deferred<Response<List<IssuePreview>>>

    @POST("api/issues")
    fun createIssue(
        @Body issue: RawIssue
    ): Deferred<Response<Int>>

    @GET("api/issues/{issueId}")
    fun getIssue(@Path("issueId") issueId: Int): Deferred<Response<Issue>>

}