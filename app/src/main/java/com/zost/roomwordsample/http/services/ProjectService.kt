package com.zost.roomwordsample.http.services

import com.zost.roomwordsample.models.Project
import kotlinx.coroutines.Deferred
import retrofit2.Response
import retrofit2.http.GET

interface ProjectService {

    @GET("api/projects/my")
    fun getMyProjects(): Deferred<Response<List<Project>>>
}