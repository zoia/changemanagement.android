package com.zost.roomwordsample.http.interceptors

import com.zost.roomwordsample.App
import okhttp3.Interceptor
import okhttp3.Response

class HeaderInterceptor : Interceptor {

    companion object {
        private const val API_VERSION = 1
    }

    override fun intercept(chain: Interceptor.Chain): Response {

        val requestBuilder = chain.request().newBuilder()
            .addHeader("Authorization", "Bearer ${App.preferences.accessToken}")
            .addHeader("Accept", "application/vnd.iman.v$API_VERSION+json, application/json, text/plain, */*`")

        return chain.proceed(requestBuilder.build())
    }

}