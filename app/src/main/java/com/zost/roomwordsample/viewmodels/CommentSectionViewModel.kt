package com.zost.roomwordsample.viewmodels

import android.app.Application
import androidx.lifecycle.MutableLiveData
import com.zost.roomwordsample.http.Api
import com.zost.roomwordsample.infrastructure.communication.Resource
import com.zost.roomwordsample.infrastructure.communication.Status
import com.zost.roomwordsample.infrastructure.default
import com.zost.roomwordsample.models.Comment
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class CommentSectionViewModel(application: Application) : BaseViewModel(application) {

    val comments = MutableLiveData<List<Comment>>().default(emptyList())

    fun addComment(issueId: Int, comment:String) {
        isLoading.value = true
        viewModelScope.launch(Dispatchers.IO) {
            val networkData: Resource<Comment> = call {
                client = Api.commentsService.addComment(issueId, comment)
            }

            withContext(Dispatchers.Main) {
                isLoading.value = false
                when (networkData.status) {
                    Status.ERROR -> showToast(networkData.message)
                    Status.SUCCESS -> {
                        networkData.data?.let {
                            comments.value = listOf(it).plus(comments.value!!)
                        }
                    }
                }
            }
        }
    }

    fun loadComments(issueId: Int) {
        isLoading.value = true
        viewModelScope.launch(Dispatchers.IO) {
            val networkData: Resource<List<Comment>> = call {
                client = Api.commentsService.getComments(issueId)
            }

            withContext(Dispatchers.Main) {
                isLoading.value = false
                when (networkData.status) {
                    Status.ERROR -> showToast(networkData.message)
                    Status.SUCCESS -> {
                        networkData.data?.let {
                            comments.value = it
                        }
                    }
                }
            }
        }
    }

}
