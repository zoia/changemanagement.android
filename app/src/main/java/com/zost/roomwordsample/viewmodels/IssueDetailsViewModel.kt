package com.zost.roomwordsample.viewmodels

import android.app.Application
import androidx.lifecycle.MutableLiveData
import com.zost.roomwordsample.http.Api
import com.zost.roomwordsample.infrastructure.Event
import com.zost.roomwordsample.infrastructure.communication.Resource
import com.zost.roomwordsample.infrastructure.communication.Status
import com.zost.roomwordsample.infrastructure.default
import com.zost.roomwordsample.models.AttributeType
import com.zost.roomwordsample.models.Issue
import com.zost.roomwordsample.models.IssueAttributeValueViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.text.SimpleDateFormat

class IssueDetailsViewModel(application: Application) : BaseViewModel(application) {

    val summary = MutableLiveData<String>()
    val status = MutableLiveData<String>()
    val description = MutableLiveData<String>()
    val assignee = MutableLiveData<IssueAttributeValueViewModel>()
    val issueId = MutableLiveData<Int>()
    val attributes = MutableLiveData<List<IssueAttributeValueViewModel>>().default(emptyList())

    val showComments = MutableLiveData<Boolean>().default(false)
    val commentsRequested: MutableLiveData<Event<Int>> = MutableLiveData()

    fun loadIssue(issueId: Int) {

        isLoading.value = true
        viewModelScope.launch(Dispatchers.IO) {
            val networkData: Resource<Issue> = call {
                client = Api.issueService.getIssue(issueId)
            }
            withContext(Dispatchers.Main) {
                isLoading.value = false
                when (networkData.status) {
                    Status.ERROR -> {
                        showToast(networkData.message)
                    }
                    Status.SUCCESS -> {
                        networkData.data?.let { issue ->
                            summary.value = issue.summary
                            description.value = issue.description
                            status.value = issue.issueStatus.label

                            issue.assigneeName?.let {
                                assignee.value = IssueAttributeValueViewModel("Призначений для", it)
                            }

                            this@IssueDetailsViewModel.issueId.value = issue.id
                            attributes.value = issue.issueAttributeValues.map {
                                IssueAttributeValueViewModel(
                                    it.attribute.label,
                                    when (it.attribute.attributeType) {
                                        AttributeType.DATE -> SimpleDateFormat("dd/MM/yyyy HH:mm").format(it.dateValue)
                                        AttributeType.NUMBER -> it.numberValue.toString()
                                        AttributeType.SELECTABLE -> it.attributeAllowedValue!!.label
                                        AttributeType.STRING -> it.stringValue!!
                                    }
                                )
                            }
                        }
                    }
                }
            }
        }
    }

    fun goToComments() {
        commentsRequested.value = Event(issueId.value!!)
    }

}