package com.zost.roomwordsample.viewmodels

import android.app.Application
import androidx.annotation.UiThread
import androidx.lifecycle.MutableLiveData
import arrow.data.Nel
import arrow.data.Validated
import arrow.data.fix
import arrow.instances.nonemptylist.semigroup.semigroup
import arrow.instances.validated.applicative.applicative
import com.google.firebase.messaging.FirebaseMessaging
import com.zost.roomwordsample.App
import com.zost.roomwordsample.R
import com.zost.roomwordsample.http.Api
import com.zost.roomwordsample.infrastructure.Event
import com.zost.roomwordsample.infrastructure.communication.Resource
import com.zost.roomwordsample.infrastructure.communication.Status
import com.zost.roomwordsample.infrastructure.decodeToken
import com.zost.roomwordsample.infrastructure.validateRequired
import com.zost.roomwordsample.models.LoginForm
import com.zost.roomwordsample.models.TokenResponse
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class LoginViewModel(application: Application) : BaseViewModel(application) {

    val login: MutableLiveData<String> = MutableLiveData()
    val password: MutableLiveData<String> = MutableLiveData()

    val loginError: MutableLiveData<String?> = MutableLiveData()
    val passwordError: MutableLiveData<String?> = MutableLiveData()

    val successfullyAuthenticated: MutableLiveData<Event<Boolean>> = MutableLiveData()

    init {
        login.value = "pkarat"
        password.value = "1234567"
    }

    private fun validate(): Validated<Nel<String>, LoginForm> {

        return Validated.applicative<Nel<String>>(Nel.semigroup())
            .map(
                validateLogin(),
                validatePassword()
            ) {
                LoginForm(it.a, it.b)
            }.fix()
    }

    fun validatePassword(): Validated<Nel<String>, String> {
        val validationResult = password.value.validateRequired(string(R.string.login_error_message))
        validationResult.fold({
            passwordError.value = string(R.string.password_error_message)
        }, {
            passwordError.value = null
        })

        return validationResult
    }

    fun validateLogin(): Validated<Nel<String>, String> {
        val validationResult = login.value.validateRequired(string(R.string.password_error_message))
        validationResult.fold({
            loginError.value = string(R.string.login_error_message)
        }, {
            loginError.value = null
        })

        return validationResult
    }

    @UiThread
    fun login() {
        val validationResult = validate()
        validationResult.fold({ { } }) { loginData ->
            {
                isLoading.value = true

                viewModelScope.launch(Dispatchers.IO) {
                    val networkData: Resource<TokenResponse> = call {
                        client = Api.authService.login(loginData.login, loginData.password)
                    }

                    withContext(Dispatchers.Main) {
                        isLoading.value = false

                        when (networkData.status) {
                            Status.ERROR -> {
                                showToast(networkData.message)
                            }
                            Status.SUCCESS -> {

                                if (!App.preferences.id.isNullOrEmpty()) {
                                    FirebaseMessaging.getInstance().unsubscribeFromTopic(App.preferences.id)
                                }

                                val tokenData = networkData.data
                                tokenData?.let { response ->
                                    App.preferences.accessToken = response.accessToken
                                    App.preferences.refreshToken = response.refreshToken

                                    val user = response.idToken.decodeToken()
                                    App.preferences.setUser(user)

                                    successfullyAuthenticated.value = Event(true)

                                    FirebaseMessaging.getInstance().subscribeToTopic(user.id)
                                }
                            }
                        }
                    }
                }
            }
        }.invoke()
    }
}