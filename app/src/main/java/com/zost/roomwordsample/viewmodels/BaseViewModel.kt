package com.zost.roomwordsample.viewmodels

import android.app.Application
import android.widget.Toast
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.zost.roomwordsample.App
import com.zost.roomwordsample.infrastructure.communication.Resource
import com.zost.roomwordsample.infrastructure.communication.CallHandler
import com.zost.roomwordsample.infrastructure.communication.call
import com.zost.roomwordsample.infrastructure.default
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Job

open class BaseViewModel(application: Application) : AndroidViewModel(application) {

    val isLoading: MutableLiveData<Boolean> = MutableLiveData<Boolean>().default(false)

    private var parentJob = Job()

    protected val viewModelScope = CoroutineScope(parentJob + kotlinx.coroutines.Dispatchers.Main)

    override fun onCleared() {
        super.onCleared()
        parentJob.cancel()
    }

    protected fun showToast(message: String?) {
        Toast.makeText(super.getApplication(), message, Toast.LENGTH_LONG).show()
    }

    suspend fun <TResponse : Any, TData : Any> callAndMap(
        block: CallHandler<TResponse, TData>.() -> Unit
    ): Resource<TData> = call(viewModelScope, block)

    suspend fun <TData : Any> call(
        block: CallHandler<TData, TData>.() -> Unit
    ): Resource<TData> = callAndMap(block)

    fun string(id: Int) = super.getApplication<App>().getString(id)

}