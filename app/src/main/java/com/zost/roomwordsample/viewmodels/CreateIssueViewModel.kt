package com.zost.roomwordsample.viewmodels

import android.app.Application
import androidx.databinding.Observable
import androidx.databinding.ObservableField
import androidx.lifecycle.MutableLiveData
import arrow.data.Nel
import arrow.data.Validated
import com.zost.roomwordsample.App
import com.zost.roomwordsample.http.Api
import com.zost.roomwordsample.infrastructure.Event
import com.zost.roomwordsample.infrastructure.communication.Resource
import com.zost.roomwordsample.infrastructure.communication.Status
import com.zost.roomwordsample.infrastructure.getOrThrow
import com.zost.roomwordsample.infrastructure.validateRequired
import com.zost.roomwordsample.models.*
import com.zost.roomwordsample.ui.activities.DropDownItem
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.util.*

class CreateIssueViewModel(application: Application) : BaseViewModel(application) {

    val issueTypes: MutableLiveData<List<DropDownItem>> = MutableLiveData()
    val selectedIssueType: ObservableField<DropDownItem> = ObservableField()
    val attributes: MutableLiveData<List<IssueTypeAttribute>> = MutableLiveData()

    val issueCreated: MutableLiveData<Event<Int>> = MutableLiveData()
    val validationRequested: MutableLiveData<Event<Boolean>> = MutableLiveData()
    val validationPerformed: ObservableField<Event<List<IssueAttributeValue>?>> = ObservableField()

    val summary: MutableLiveData<String> = MutableLiveData()
    val description: MutableLiveData<String> = MutableLiveData()

    val projects: MutableLiveData<List<DropDownItem>> = MutableLiveData()

    val summaryError: MutableLiveData<String?> = MutableLiveData()
    val descriptionError: MutableLiveData<String?> = MutableLiveData()

    init {
        loadProjects()

        selectedIssueType.addOnPropertyChangedCallback(
            object : Observable.OnPropertyChangedCallback() {
                override fun onPropertyChanged(p0: Observable, p1: Int) {
                    onIssueTypeChanged()
                }
            })

        validationPerformed.addOnPropertyChangedCallback(
            object : Observable.OnPropertyChangedCallback() {
                override fun onPropertyChanged(sender: Observable?, propertyId: Int) {

                    val summaryValidated = validateSummary()
                    val descValidated = validateDescription()

                    if (summaryValidated.isInvalid || descValidated.isInvalid) {
                        return
                    }

                    val formValid = sender as ObservableField<Event<List<IssueAttributeValue>?>>
                    val validatedAttributes = formValid.get()?.peekContent() ?: return
                    val project = validatedAttributes.find { it.attributeId == -1 }

                    val issue = RawIssue(
                        summaryValidated.getOrThrow(),
                        descValidated.getOrThrow(),
                        selectedIssueType.get()!!.id!!,
                        project!!.attributeAllowedValueId!!,
                        validatedAttributes.filter { it.attributeId >= 0 }
                    )

                    isLoading.value = true
                    viewModelScope.launch(Dispatchers.IO) {
                        val networkData: Resource<Int> = call {
                            client = Api.issueService.createIssue(issue)
                        }

                        withContext(Dispatchers.Main) {
                            isLoading.value = false
                            when (networkData.status) {
                                Status.ERROR -> {
                                    showToast(networkData.message)
                                }
                                Status.SUCCESS -> {
                                    issueCreated.value = Event(networkData.data!!)
                                }
                            }
                        }
                    }
                }
            }

        )
    }

    private fun loadProjects() {
        isLoading.value = true
        viewModelScope.launch(Dispatchers.IO) {
            val networkData: Resource<List<Project>> = call {
                client = Api.projectService.getMyProjects()
            }

            withContext(Dispatchers.Main) {
                isLoading.value = false
                when (networkData.status) {
                    Status.ERROR -> {
                        showToast(networkData.message)
                    }
                    Status.SUCCESS -> {
                        val projects = networkData.data!!.map { DropDownItem(it.id, it.name) }

                        val withoutDefault = projects.filter { it.id != App.preferences.defaultProjectId }
                        val default = projects.find { it.id == App.preferences.defaultProjectId }
                        val selected = default ?: DropDownItem(null, "Оберіть проект")

                        val result = LinkedList(withoutDefault)
                        result.push(selected)
                        this@CreateIssueViewModel.projects.value = result
                    }
                }
            }
        }
    }

    private fun onIssueTypeChanged() {
        val issueTypeId = selectedIssueType.get()?.id!!

        isLoading.value = true
        viewModelScope.launch(Dispatchers.IO) {
            val networkData: Resource<List<IssueTypeAttribute>> = call {
                client = Api.issueTypeService.getIssueTypesAttributes(issueTypeId)
            }

            withContext(Dispatchers.Main) {
                isLoading.value = false
                when (networkData.status) {
                    Status.ERROR -> {
                        showToast(networkData.message)
                    }
                    Status.SUCCESS -> {
                        val types = networkData.data!!
                        attributes.value = types
                    }
                }
            }
        }
    }

    fun createIssue() {
        validationRequested.value = Event(true)
    }

    private fun clearErrors() {
        summaryError.value = null
        descriptionError.value = null
    }

    fun loadIssueTypes() {

        isLoading.value = true
        viewModelScope.launch(Dispatchers.IO) {
            val networkData: Resource<ListResult<IssueType>> = call {
                client = Api.issueTypeService.getIssueTypes()
            }

            withContext(Dispatchers.Main) {
                isLoading.value = false

                when (networkData.status) {
                    Status.ERROR -> {
                        showToast(networkData.message)
                    }
                    Status.SUCCESS -> {
                        clearErrors()
                        val types = networkData.data!!
                        issueTypes.value = types.items.map {
                            DropDownItem(it.id, it.label)
                        }
                        selectedIssueType.set(issueTypes.value?.first())
                    }
                }
            }
        }
    }

    fun validateSummary(): Validated<Nel<String>, String> {
        val validationResult = summary.value.validateRequired("Будь ласка, вкажіть назву")
        validationResult.fold({
            summaryError.value = it.head
        }, {
            summaryError.value = null
        })

        return validationResult
    }

    fun validateDescription(): Validated<Nel<String>, String> {
        val validationResult = description.value.validateRequired("Будь ласка, вкажіть опис")
        validationResult.fold({
            descriptionError.value = it.head
        }, {
            descriptionError.value = null
        })

        return validationResult
    }

}