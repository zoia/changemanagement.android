package com.zost.roomwordsample.viewmodels

import android.app.Application
import androidx.lifecycle.MutableLiveData
import com.zost.roomwordsample.App
import com.zost.roomwordsample.http.Api
import com.zost.roomwordsample.infrastructure.Event
import com.zost.roomwordsample.infrastructure.communication.Resource
import com.zost.roomwordsample.infrastructure.communication.Status
import com.zost.roomwordsample.infrastructure.default
import com.zost.roomwordsample.models.IssuePreview
import com.zost.roomwordsample.models.UserViewModel
import com.zost.roomwordsample.ui.adapters.ItemClickedListener
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class IssueListViewModel(application: Application) : BaseViewModel(application) {

    val user = UserViewModel(
        App.preferences.fullName,
        App.preferences.email,
        App.preferences.phoneNumber,
        App.preferences.jobTitle
    )

    val issues = MutableLiveData<List<IssuePreview>>().default(emptyList())

    val issueSelected = MutableLiveData<Event<Int>>()

    val itemSelectedListener: ItemClickedListener<IssuePreview> = object : ItemClickedListener<IssuePreview> {
        override fun onItemClicked(item: IssuePreview) {
            issueSelected.value = Event(item.id)
        }
    }

    fun load() {

        isLoading.value = true
        viewModelScope.launch(Dispatchers.IO) {
            val networkData: Resource<List<IssuePreview>> = call {
                client = Api.issueService.getIssueList()
            }

            withContext(Dispatchers.Main) {
                isLoading.value = false
                when (networkData.status) {
                    Status.ERROR -> {
                        showToast(networkData.message)
                    }
                    Status.SUCCESS -> {
                        issues.value = networkData.data
                    }
                }
            }
        }
    }

}