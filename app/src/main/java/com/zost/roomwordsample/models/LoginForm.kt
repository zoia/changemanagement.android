package com.zost.roomwordsample.models

data class LoginForm(val login: String, val password: String)