package com.zost.roomwordsample.models

class IssuePreview(
    var id: Int,
    var summary: String,
    var description: String,
    var project: String,
    var status: String,
    var type: String
)

class RawIssue(
    var summary: String,
    var description: String,
    var issueTypeId: Int,
    var projectId: Int,
    var issueAttributeValues: List<IssueAttributeValue>
)

class Issue(
    var id: Int,
    var summary: String,
    var description: String,
    var issueStatus: IssueStatus,
    var issueType: IssueType,
    var project: Project,
    var assigneeName: String?,
    var issueAttributeValues: List<IssueProperty>
)