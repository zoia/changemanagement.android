package com.zost.roomwordsample.models

import java.util.*

data class Comment(
    val id: Int,
    val commentText: String,
    val createdDate: Date,
    val createdByUser: User
)