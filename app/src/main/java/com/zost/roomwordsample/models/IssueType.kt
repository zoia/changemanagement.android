package com.zost.roomwordsample.models

import com.google.gson.annotations.SerializedName
import java.util.*

enum class AttributeType {
    @SerializedName("0")
    STRING,
    @SerializedName("1")
    DATE,
    @SerializedName("2")
    NUMBER,
    @SerializedName("3")
    SELECTABLE
}

data class AttributeAllowedValue(
    var id: Int,
    var label: String
)

data class Attribute(
    var attributeId: Int,
    var label: String,
    var attributeType: AttributeType,
    var attributeAllowedValues: ArrayList<AttributeAllowedValue>
)

data class IssueTypeAttribute(
    var issueTypeId: Int,
    var isRequired: Boolean,
    var attributeId: Int,
    var attribute: Attribute
)

data class IssueType(
    var id: Int,
    var label: String,
    var issueTypeAttributes: ArrayList<IssueTypeAttribute>
) {
    override fun toString() = label
}

data class IssueStatus(
    var id: Int,
    var label: String
)

open class IssueAttributeValue(
    var attributeId: Int,
    var stringValue: String? = null,
    var dateValue: Date? = null,
    var numberValue: Double? = null,
    var attributeAllowedValueId: Int? = null
)

class IssueProperty(
    attributeId: Int,
    stringValue: String?,
    dateValue: Date?,
    numberValue: Double?,
    attributeAllowedValueId: Int?,
    var attribute: Attribute,
    var attributeAllowedValue: AttributeAllowedValue?
) : IssueAttributeValue(attributeId, stringValue, dateValue, numberValue, attributeAllowedValueId)

class IssueAttributeValueViewModel(
    var label: String,
    var value: String
)

data class Project(
    var id: Int,
    var name: String,
    var description: String
)