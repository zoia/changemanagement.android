package com.zost.roomwordsample.models

import com.google.gson.annotations.SerializedName

data class UserViewModel(
    val fullName: String,
    val email: String,
    val phone: String,
    val jobTitle: String
)

class TokenUser(
    @SerializedName("sub")
    val id: String,
    val name: String,
    @SerializedName("fullname")
    val fullName: String,
    val email: String,
    @SerializedName("jobtitle")
    val jobTitle: String,
    val phone: String,
    var defaultProjectId: Int?
)

class User(
    val id: String?,
    val name: String?,
    val fullName: String?,
    val email: String?,
    val profileImageFilename: String?
)

data class Configuration(
    val defaultProject: Int?
)