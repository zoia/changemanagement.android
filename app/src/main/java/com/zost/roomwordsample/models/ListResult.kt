package com.zost.roomwordsample.models

data class ListResult<TEntity>(var items: List<TEntity>, var total: Int)