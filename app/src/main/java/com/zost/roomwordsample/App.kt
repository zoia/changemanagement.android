package com.zost.roomwordsample

import android.app.Application
import com.zost.roomwordsample.infrastructure.Prefs
import org.acra.ACRA
import org.acra.ReportingInteractionMode
import org.acra.annotation.ReportsCrashes

@ReportsCrashes(mailTo = "ostapyukzoya@outlook.com",
    mode = ReportingInteractionMode.TOAST,
    resToastText = R.string.error_message)
class App : Application() {
    companion object {

        @Volatile
        private var prefs: Prefs? = null

        val preferences: Prefs
            get() = prefs!!
    }

    override fun onCreate() {
        prefs = Prefs(applicationContext)
        ACRA.init(this)
        super.onCreate()
    }
}